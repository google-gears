// Copyright 2008, Google Inc.
//
// Redistribution and use in source and binary forms, with or without 
// modification, are permitted provided that the following conditions are met:
//
//  1. Redistributions of source code must retain the above copyright notice, 
//     this list of conditions and the following disclaimer.
//  2. Redistributions in binary form must reproduce the above copyright notice,
//     this list of conditions and the following disclaimer in the documentation
//     and/or other materials provided with the distribution.
//  3. Neither the name of Google Inc. nor the names of its contributors may be
//     used to endorse or promote products derived from this software without
//     specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
// WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
// MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
// EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF 
// ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import "oaidl.idl";
import "ocidl.idl";

#ifdef OFFICIAL_BUILD
// The Image API has not been finalized for official builds
#else

[
  object,
  uuid(d11da9d8-3f32-42ed-b201-d50411eb7696),
  dual,
  nonextensible,
  pointer_default(unique)
]
interface GearsImageInterface : IDispatch {
  HRESULT resize([in] int width,
                 [in] int height);
  HRESULT crop([in] int x,
               [in] int y,
               [in] int width,
               [in] int height);
  HRESULT rotate([in] int degrees);
  HRESULT flipHorizontal();
  HRESULT flipVertical();
  HRESULT drawImage([in] IUnknown *image,
                    [in] int x,
                    [in] int y);
  [propget] HRESULT width([out, retval] int *retval);
  [propget] HRESULT height([out, retval] int *retval);
  HRESULT toBlob([in, optional] const VARIANT *type,
                 [out, retval] IUnknown **retval);
};

[
  object,
  uuid(3f1d2211-43dc-481c-b52c-eca819ec6680),
  dual,
  nonextensible,
  pointer_default(unique)
]
interface GearsImageLoaderInterface : IDispatch {
  HRESULT createImageFromBlob([in] IUnknown *blob,
                              [out, retval] GearsImageInterface **retval);
};

[
  object,
  uuid(30f1377f-7061-469f-9179-311c08619b3d),
  nonextensible,
  pointer_default(unique)
]
interface GearsImagePvtInterface : IUnknown {
  // retval->ullVal is assigned the pointer to the wrapped Image object.
  [propget] HRESULT contents([out, retval] VARIANT *retval);
};

#endif // not OFFICIAL_BUILD
