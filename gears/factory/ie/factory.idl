// Copyright 2007, Google Inc.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
//  1. Redistributions of source code must retain the above copyright notice,
//     this list of conditions and the following disclaimer.
//  2. Redistributions in binary form must reproduce the above copyright notice,
//     this list of conditions and the following disclaimer in the documentation
//     and/or other materials provided with the distribution.
//  3. Neither the name of Google Inc. nor the names of its contributors may be
//     used to endorse or promote products derived from this software without
//     specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
// WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
// EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
// ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import "oaidl.idl";
import "ocidl.idl";


//
// GearsFactoryInterface
//

[
  object,
  uuid(CADD7DF9-B7AF-426D-AE24-5E00CB8CF982),
  dual,
  nonextensible,
  pointer_default(unique)
]
interface GearsFactoryInterface : IDispatch {
  HRESULT create([in] const BSTR class_name,
                 [in, optional] const VARIANT *class_version,
                 [out, retval] IDispatch **retval);
  HRESULT getBuildInfo([out, retval] BSTR *retval);
  [propget] HRESULT version([out, retval] BSTR *retval);

#ifdef WINCE
  // Hold WinCE feature set at version 0.2 for now.
#else
  HRESULT getPermission([in, optional] const VARIANT *site_name,
                        [in, optional] const VARIANT *image_url,
                        [in, optional] const VARIANT *extra_message,
                        [out, retval] VARIANT_BOOL *retval);
  [propget] HRESULT hasPermission([out, retval] VARIANT_BOOL *retval);
#endif
#ifdef WINCE
  HRESULT privateSetGlobalObject([in] IDispatch *js_dispatch);
#endif
};
