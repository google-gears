/* ***** BEGIN LICENSE BLOCK *****
 * Version: NPL 1.1/GPL 2.0/LGPL 2.1
 *
 * The contents of this file are subject to the Netscape Public License
 * Version 1.1 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * http://www.mozilla.org/NPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is mozilla.org code.
 *
 * The Initial Developer of the Original Code is
 * Netscape Communications Corporation.
 * Portions created by the Initial Developer are Copyright (C) 1998
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"), or
 * the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
 * in which case the provisions of the GPL or the LGPL are applicable instead
 * of those above. If you wish to allow use of your version of this file only
 * under the terms of either the GPL or the LGPL, and not to allow others to
 * use your version of this file under the terms of the NPL, indicate your
 * decision by deleting the provisions above and replace them with the notice
 * and other provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file under
 * the terms of any one of the NPL, the GPL or the LGPL.
 *
 * ***** END LICENSE BLOCK ***** */

//////////////////////////////////////////////////////////////
//
// Main plugin entry point implementation
//
#include "gears/base/common/base_class.h"
#include "gears/base/common/thread_locals.h"
#include "gears/base/npapi/module.h"

#ifndef HIBYTE
#define HIBYTE(x) ((((uint32)(x)) & 0xff00) >> 8)
#endif

// Export NPAPI entry points on OS X.
#ifdef BROWSER_WEBKIT
// Define this to keep prototypes consistent between NPAPI implementations.
#define OSCALL

#pragma export on
extern "C" {
  // Mach-o entry points
  NPError NP_Initialize(NPNetscapeFuncs *browserFuncs);
  NPError NP_GetEntryPoints(NPPluginFuncs *pluginFuncs);
  void NP_Shutdown(void);
  // For compatibility with NPAPI in Opera & FF on the Mac, we need to implement
  // this.
  // int main(NPNetscapeFuncs *browserFuncs, NPPluginFuncs *pluginFuncs,
  //         void *shutdown);
}
#pragma export off
#endif  // BROWSER_WEBKIT

#ifdef WIN32
#include "gears/base/ie/atl_headers.h"
// Initialize ATL.
// TODO(mpcomplete): remove when no longer needed.  This is used by HttpRequest
// for now.
CComModule g_module;

// TODO(mpcomplete): remove IE dependency.
// This is defined in gears/base/common/message_queue_ie.h.  It should only be
// called here.
void ShutdownThreadMessageQueue();
#endif;


// Store the browser functions in thread local storage to avoid calling the
// functions on a different thread.
static NPNetscapeFuncs g_browser_funcs;
const std::string kNPNFuncsKey("base:NPNetscapeFuncs");

NPError OSCALL NP_GetEntryPoints(NPPluginFuncs* funcs)
{
  if (funcs == NULL)
    return NPERR_INVALID_FUNCTABLE_ERROR;

  // On FF & Safari on win32, funcs->size is a parameter we need to check on
  // input in order to make sure we're being passed a structure of the right
  // size.
  //
  // Webkit under OSX on the other hand, passes 0 in funcs->size.
  // Apple's sample code (NetscapeMoviePlugIn) treats this as an output
  // parameter.
  //
  // We play it safe by being consistent with Apple's example code under OSX &
  // keeping with the standard behavior otherwise.
#ifdef BROWSER_WEBKIT
  funcs->size          = sizeof(NPPluginFuncs);
#else
  if (funcs->size < sizeof(NPPluginFuncs))
    return NPERR_INVALID_FUNCTABLE_ERROR;
#endif
  funcs->version       = (NP_VERSION_MAJOR << 8) | NP_VERSION_MINOR;
  funcs->newp          = NPP_New;
  funcs->destroy       = NPP_Destroy;
  funcs->setwindow     = NPP_SetWindow;
  funcs->newstream     = NPP_NewStream;
  funcs->destroystream = NPP_DestroyStream;
  funcs->asfile        = NPP_StreamAsFile;
  funcs->writeready    = NPP_WriteReady;
#ifdef BROWSER_WEBKIT
  funcs->write         = reinterpret_cast<NPP_WriteProcPtr>(NPP_Write);
#else
  funcs->write         = NPP_Write;
#endif
  funcs->print         = NPP_Print;
  funcs->event         = NPP_HandleEvent;
  funcs->urlnotify     = NPP_URLNotify;
  funcs->getvalue      = NPP_GetValue;
  funcs->setvalue      = NPP_SetValue;
  funcs->javaClass     = NULL;

  return NPERR_NO_ERROR;
}

NPError OSCALL NP_Initialize(NPNetscapeFuncs* funcs)
{
  if (funcs == NULL)
    return NPERR_INVALID_FUNCTABLE_ERROR;

  if (HIBYTE(funcs->version) > NP_VERSION_MAJOR)
    return NPERR_INCOMPATIBLE_VERSION_ERROR;

  if (funcs->size < sizeof(NPNetscapeFuncs))
    return NPERR_INVALID_FUNCTABLE_ERROR;

  g_browser_funcs = *funcs;
  ThreadLocals::SetValue(kNPNFuncsKey, &g_browser_funcs, NULL);

  return NPERR_NO_ERROR;
}


// Apple's NetscapeMoviePlugin Example defines NP_Shutdown this as returning a
// void.
// Gecko defines this differently.
#ifdef BROWSER_WEBKIT
void OSCALL NP_Shutdown()
#else
NPError OSCALL NP_Shutdown()
#endif
{
#ifdef WIN32
  // We're being unloaded, but the thread isn't necessarily detached.  Force the
  // thread shutdown handling anyway.
  MyDllMain(0, DLL_THREAD_DETACH, 0);
#endif

#ifdef BROWSER_WEBKIT
// void return type in Webkit.
#else
  return NPERR_NO_ERROR;
#endif
}

#ifdef WIN32
BOOL MyDllMain(HANDLE instance, DWORD reason, LPVOID reserved) {
  switch (reason) {
    case DLL_THREAD_DETACH:
      ShutdownThreadMessageQueue();
      ThreadLocals::HandleThreadDetached();
      break;
    case DLL_PROCESS_DETACH:
      ThreadLocals::HandleProcessDetached();
      break;
    case DLL_PROCESS_ATTACH:
      ThreadLocals::HandleProcessAttached();
      break;
  }

  return TRUE;
}

extern "C"
BOOL WINAPI DllMain(HANDLE instance, DWORD reason, LPVOID reserved) {
 return MyDllMain(instance, reason, reserved);
}
#endif
