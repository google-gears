// Copyright 2007, Google Inc.
//
// Redistribution and use in source and binary forms, with or without 
// modification, are permitted provided that the following conditions are met:
//
//  1. Redistributions of source code must retain the above copyright notice, 
//     this list of conditions and the following disclaimer.
//  2. Redistributions in binary form must reproduce the above copyright notice,
//     this list of conditions and the following disclaimer in the documentation
//     and/or other materials provided with the distribution.
//  3. Neither the name of Google Inc. nor the names of its contributors may be
//     used to endorse or promote products derived from this software without
//     specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
// WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
// MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
// EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF 
// ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#import <WebKit/WebKit.h>

#import "gears/base/common/string_utils.h"
#import "gears/base/safari/string_utils.h"
#import "gears/third_party/scoped_ptr/scoped_ptr.h"

@implementation NSString(GearsString16Conversion)
//------------------------------------------------------------------------------
+ (NSString *)stringWithString16:(const char16 *)str {
  return [(NSString *)CFStringCreateWithString16(str) autorelease];
}

//------------------------------------------------------------------------------
- (bool)string16:(std::string16 *)out16 {
  const char *utf8 = [self UTF8String];
  
  if (!strlen(utf8)) {
    out16->empty();
    return true;
  }
  
  return(UTF8ToString16(utf8, out16));
}

//------------------------------------------------------------------------------
- (UniChar *)copyCharacters {
  int len = [self length];
  UniChar *buffer = new UniChar[len + 1];

  if (len)
    [self getCharacters:buffer];
  
  buffer[len] = 0;
  
  return buffer;
}

@end

//------------------------------------------------------------------------------
bool CFStringRefToString16(CFStringRef str, std::string16 *out16) {
  if (!str || !out16)
    return false;
  
  unsigned long length = CFStringGetLength(str);
  const UniChar *outStr = CFStringGetCharactersPtr(str);

  if (!outStr) {
    scoped_array<UniChar> buffer(new UniChar[length + 1]);
    CFStringGetCharacters(str, CFRangeMake(0, length), buffer.get());
    buffer[length] = 0;
    out16->assign(buffer.get());
  } else {
    out16->assign(outStr);
  }
  
  return true;
}

//------------------------------------------------------------------------------
CFStringRef CFStringCreateWithString16(const char16 *str) {
  if (!str)
    return CFSTR("");
  
  return CFStringCreateWithCharacters(NULL, str, char16_wcslen(str));
}
  
//------------------------------------------------------------------------------
#if DEBUG
// Only for debugging -- leaks memory
const char *DebugString16(const char16 *str) {
  std::string *out8(new std::string);
  String16ToUTF8(str, out8);
  return out8->c_str();
}
#endif
