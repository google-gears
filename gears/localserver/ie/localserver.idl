// Copyright 2006, Google Inc.
//
// Redistribution and use in source and binary forms, with or without 
// modification, are permitted provided that the following conditions are met:
//
//  1. Redistributions of source code must retain the above copyright notice, 
//     this list of conditions and the following disclaimer.
//  2. Redistributions in binary form must reproduce the above copyright notice,
//     this list of conditions and the following disclaimer in the documentation
//     and/or other materials provided with the distribution.
//  3. Neither the name of Google Inc. nor the names of its contributors may be
//     used to endorse or promote products derived from this software without
//     specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
// WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
// MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
// EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF 
// ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import "oaidl.idl";
import "ocidl.idl";


//
// GearsManagedResourceStoreInterface
//

[
  object,
  uuid(990DC8DD-A97D-4701-962A-53721251821C),
  dual,
  nonextensible,
  pointer_default(unique)
]
interface GearsManagedResourceStoreInterface : IDispatch {
  // Identifying properties
  [propget] HRESULT name([out, retval] BSTR *name);
  [propget] HRESULT requiredCookie([out, retval] BSTR *required_cookie);

  // Enable/disable local serving
  [propget] HRESULT enabled([out, retval] VARIANT_BOOL *enabled);
  [propput] HRESULT enabled([in] VARIANT_BOOL enabled);

  // Auto-updating
  [propget] HRESULT manifestUrl([out, retval] BSTR *manifestUrl);
  [propput] HRESULT manifestUrl([in] const BSTR manifestUrl);
  [propget] HRESULT lastUpdateCheckTime([out, retval] long *time);
  [propget] HRESULT updateStatus([out, retval] int *status);
  [propget] HRESULT lastErrorMessage([out, retval] BSTR *last_error_message);

#ifdef WINCE
  // Hold WinCE feature set at version 0.2 for now.
#else
  [propput] HRESULT onerror([in] const VARIANT *handler);
  [propput] HRESULT onprogress([in] const VARIANT *handler);
  [propput] HRESULT oncomplete([in] const VARIANT *handler);
#endif

  HRESULT checkForUpdate();
   
  // Version information 
  [propget] HRESULT currentVersion([out, retval] BSTR *ver);
};


#ifdef WINCE
  // FileSubmitter is not implemented for WinCE.
#else
//
// GearsFileSubmitterInterface
// Facilitates the inclusion of resources into form submissions
// as the file parts of multipart/form-data encoded POSTs.
//

[
  object,
  uuid(0747A88C-5033-44f7-94B4-2E2823925996),
  dual,
  nonextensible,
  pointer_default(unique)
]
interface GearsFileSubmitterInterface : IDispatch {
  HRESULT setFileInputElement(
      [in] IDispatch *file_input_element,
      [in] const BSTR resource_url);
};
#endif

//
// GearsResourceStoreInterface
//

[
  object,
  uuid(3F84AC4C-F26D-4a75-9E22-6695F6953EDD),
  dual,
  nonextensible,
  pointer_default(unique)
]
interface GearsResourceStoreInterface : IDispatch {
  // Identifying properties
  [propget] HRESULT name([out, retval] BSTR *name);
  [propget] HRESULT requiredCookie([out, retval] BSTR *required_cookie);

  // Enable/disable local serving
  [propget] HRESULT enabled([out, retval] VARIANT_BOOL *enabled);
  [propput] HRESULT enabled([in] VARIANT_BOOL enabled);

  HRESULT capture(
      [in] const VARIANT *urls, 
      [in] VARIANT *completion_callback, 
      [out, retval] long *capture_id);
  
  HRESULT abortCapture(
      [in] long capture_id);

  HRESULT isCaptured(
      [in] const BSTR url, 
      [out, retval] VARIANT_BOOL *retval);

  HRESULT remove(
      [in] const BSTR url);

  HRESULT rename(
      [in] const BSTR src_url,
      [in] const BSTR dst_url);

  HRESULT copy(
      [in] const BSTR src_url,
      [in] const BSTR dst_url);
  
  HRESULT getHeader(
      [in] const BSTR url,
      [in] const BSTR header,
      [out, retval] BSTR *value);
  
  HRESULT getAllHeaders(
      [in] const BSTR url,
      [out, retval] BSTR *all_headers);
  
#ifdef OFFICIAL_BUILD
  // Blob support is not ready for prime time yet
#else
  HRESULT captureBlob(
      [in] IUnknown *blob,
      [in] const BSTR url);
#endif  // OFFICIAL_BUILD
  
  HRESULT captureFile(
      [in] IDispatch *file_input_element,
      [in] const BSTR url);
  
  HRESULT getCapturedFileName(
      [in] const BSTR url,
      [out, retval] BSTR *file_name);

#ifdef WINCE
  // FileSubmitter is not implemented for WinCE.
#else
  HRESULT createFileSubmitter(
      [out, retval] GearsFileSubmitterInterface **file_submitter);
#endif
};


//
// GearsLocalServerInterface
//

[
  object,
  uuid(A207BB88-9F3B-4b5e-A267-234C4DDDB331),
  dual,
  nonextensible,
  pointer_default(unique)
]
interface GearsLocalServerInterface : IDispatch {
  HRESULT canServeLocally(
      [in] const BSTR url, 
      [out, retval] VARIANT_BOOL *can);

  HRESULT createManagedStore(
      [in] const BSTR name,
      [in, optional] const VARIANT *required_cookie,
      [out, retval] GearsManagedResourceStoreInterface **store);

  HRESULT openManagedStore(
      [in] const BSTR name,
      [in, optional] const VARIANT *required_cookie,
      [out, retval] GearsManagedResourceStoreInterface **store);

  HRESULT removeManagedStore(
      [in] const BSTR name,
      [in, optional] const VARIANT *required_cookie);

  HRESULT createStore(
      [in] const BSTR name,
      [in, optional] const VARIANT *required_cookie,
      [out, retval] GearsResourceStoreInterface **store);

  HRESULT openStore(
      [in] const BSTR name,
      [in, optional] const VARIANT *required_cookie,
      [out, retval] GearsResourceStoreInterface **store);

  HRESULT removeStore(
      [in] const BSTR name,
      [in, optional] const VARIANT *required_cookie);
};
